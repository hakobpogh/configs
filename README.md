# My personal configs

## MacOS

### package manager
as a package manager I prefer `brew`. to install brew please run the following command in your
terminal:
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```
if any issue please follow the instructions at [brew.sh](https://brew.sh).

### terminal

#### app
as a terminal my preferred app is [iTerm2](https://iterm2.com/). I prefer its Minimal theme
which can be activated from `Preferences > Appearance > General > Theme`.
I also like its status bar. To activate it you can go to `Preferences > Profiles > Session`
and check the `Status bar enabled` checkbox and then click the `Configure status bar`
button to configure it. As for now I prefer the following status bar components:
`Current Directory`, `Fixed-size Spacer`, `Job Name`, `Spring`, `git state`.

#### shell
as a shell I prefer `zsh` as it has some helpful fast keys and cool themes. to install it
just run `brew install zsh` and if any issues then take a look to [docs](https://github.com/ohmyzsh/ohmyzsh/wiki/Installing-ZSH).
to activate the `zsh` please run this command `chsh -s /bin/zsh`. to customize the zsh
you need to install `oh-my-zsh` as well. to do that please run the following command:
```
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

after installing `oh-my-zsh` create a file called `node.zsh-theme` in the `~/.oh-my-zsh/custom/themes/`
and write the following in the file:
```shell script
GREEN="%{$fg_bold[green]%}"
YELLOW="%{$fg_bold[yellow]%}"
CYAN="%{$fg[cyan]%}"
RED="%{$fg[red]%}"
RESET="%{$reset_color%}"
BOLD_BLUE="%{$fg_bold[blue]%}"
BLUE="%{$fg[blue]%}"
LIGHT_YELLOW="%{$fg[yellow]%}"

ZSH_THEME_GIT_PROMPT_PREFIX="$BOLD_BLUE git:($RED"
ZSH_THEME_GIT_PROMPT_SUFFIX="$RESET"
ZSH_THEME_GIT_PROMPT_DIRTY="$BLUE) $LIGHT_YELLOW✗"
ZSH_THEME_GIT_PROMPT_CLEAN="$BLUE)"

PROMPT='$GREEN⬢ $USER@%m$CYAN:%c$(git_prompt_info)$YELLOW> $RESET'
```

after that edit your `~/.zshrc` file and set `ZSH_THEME="node"`.
you can also find the line where the plugins are set and add some more plugins for
better suggestions. I'm using the following list:
```shell script
plugins=(
  git
  bundler
  dotenv
  osx
  rake
  rbenv
  ruby
  brew
  pip
  python
  colorize
  ng
  colored-man-pages
  gitignore
  glassfish
  gulp
  grudle
  gcloud
  man
  brew
  cp
  node
  yarn
  vim-interaction
  vscode
  sudo
  sublime
)
```

you can find a plugin you want and add it in the plugins list. for more info
about plugins available you can `ls ~/.oh-my-zsh/plugins/` or visit
[zsh plugins docs](https://github.com/ohmyzsh/ohmyzsh/wiki/Plugins).
